const tabs = document.getElementsByClassName('tab');

// tabsはタブの個数分存在するので、ループ処理を用いてそれぞれにクリックイベントを付与

for(let i = 0; i < tabs.length; i++) {

tabs[i].addEventListener('click', changeTab, false);

}



// addEventListenerで付与されたクリックしたときに呼び出される関数

function changeTab(){

// タブのclassを変更

// active-tabクラスによって選択されているかを判別しているため、

// タブクリック時に一度すべての選択を解除するためクラス削除

document.getElementsByClassName('active-tab')[0].classList.remove('active-tab');

// thisでクリックされた要素のみを指定することができる

// addでクリックされたタブにactive-tabを追加

this.classList.add('active-tab');

// コンテンツのclassを変更

document.getElementsByClassName('show')[0].classList.remove('show');

// クリックされたタブのクラスを取得

const tabClasses = Array.prototype.slice.call(this.classList);

// filter関数でクラス配列から、

// tab,active-tab以外のクラスを取得（japanese,mathematics,historyを取得）

var result = tabClasses.filter(function( item ) {

	return item != 'tab' && item != 'active-tab';

  });

// タブに応じた授業内容を表示

document.getElementsByClassName('content' + ' ' + result)[0].classList.add('show');

};